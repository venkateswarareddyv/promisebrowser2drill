let container = document.getElementById('root')

function toDisplayOnHtml(status, count, title) {

    return `<div class="todo-item">
                <label>
                    <input type="checkbox" class="input" ${status ? "checked" : ""}>
                    <span class="span">@${count}</span><span class="span1">${title}</span>
                </label>
            </div>`
}

function createDomElements(users, groupOfTodos) {
    let count = 0
    groupOfTodos.map((array) => {
        array.map((object) => {
            container.innerHTML += toDisplayOnHtml(object.completed, users[count], object.title)
        })
        count++
    })
}

export { createDomElements };