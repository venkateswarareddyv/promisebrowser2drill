import { createDomElements } from "./func"

function todosUsers() {
    let todoUsers = fetch('https://jsonplaceholder.typicode.com/users')
    return todoUsers
}

function requests(usersDataArr) {
    let arr = []
    usersDataArr.map(user => arr.push(fetch(`https://jsonplaceholder.typicode.com/todos?userId=${user.id}`)))
    return arr
}

todosUsers().then((usersData) => {
    return usersData.json()
}).then((usersData) => {
    return UsersTodos(usersData)
}).catch((err) => {
    console.log(err)
})


function UsersTodos(users) {
    Promise.all(requests(users))
        .then(responses => {
            return Promise.all(responses)
        }).then(responses => {
            let data = responses.map(each => each.json())
            return Promise.all(data)
        }).then((responses) => {
            return usersData(users, responses)
        }).catch((err) => {
            console.log(err)
        })
}

function usersData(users, groupOfTodos) {

    let usersName = users.map((user) => user.name)

    createDomElements(usersName, groupOfTodos);

}


